{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  ...
}: {
  imports = [
    ./git.nix
    ./bash.nix
    ./starship.nix
  ];

  nixpkgs = {
    overlays = [
      inputs.emacs-overlay.overlay
    ];
    config = {
      allowUnfree = true;
    };
  };

  home = {
    username = "jdm204";
    homeDirectory = "/home/jdm204";

    packages = with pkgs; [
      ripgrep just fd du-dust eza xsv bat duf bottom
      glab
      emacs-pgtk
      ibm-plex
      (pkgs.nerdfonts.override { fonts = [ "Iosevka" ]; })
    ];
  };

  programs = {
    home-manager.enable = true;
    zoxide.enable = true;
  };
  
  fonts.fontconfig.enable = true;

  systemd.user.startServices = "sd-switch";

  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  home.stateVersion = "23.05";
}
