{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.services.myfonts;
in {
  options.services.myfonts = {
    enable = mkEnableOption "my favourite fonts";
  };
  config = mkIf cfg.enable {
    fonts.fontconfig.enable = true;
    home.packages = with pkgs; [
      ibm-plex alegreya overpass inter
      (pkgs.nerdfonts.override { fonts = [ "SourceCodePro" "Iosevka" ]; })      
    ];
  };
}
