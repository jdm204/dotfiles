{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.services.myemacs;
in {
  options.services.myemacs = {
    enable = mkEnableOption "my emacs configuration";
  };
  config = mkIf cfg.enable {
    nixpkgs.overlays = [
      (self: super: rec {
        isync = super.isync.override { withCyrusSaslXoauth2 = true; };
      })
    ];
    fonts.fontconfig.enable = true;

    programs.gpg = {
      enable = true;
      settings.no-symkey-cache = false;
    };
    services.gpg-agent = {
      enable = true;
      pinentryPackage = pkgs.pinentry-qt;
      defaultCacheTtl = 86400;
      maxCacheTtl = 86400;
    };

    services.emacs = {
      enable = true;
      package = pkgs.emacs-unstable-pgtk;
      defaultEditor = true;
    };

    services.mbsync = {
      enable = true;
      package = pkgs.isync;
      postExec = "${pkgs.mu}/bin/mu index";
    };

    home.packages = with pkgs; [
      ((emacsPackagesFor emacs-unstable-pgtk).emacsWithPackages
        (epkgs: [ epkgs.mu4e ]))
      emacs-all-the-icons-fonts mu msmtp isync # https://github.com/ldangelo/nix/blob/main/modules/home-manager/default.nix this isync doesn't support oauth2, check this repo, find the isync override
      emacsPackages.mu4e
      (aspellWithDicts (dicts: with dicts; [
        en
        en-computers
        en-science
      ]))
    ];
  };
}
