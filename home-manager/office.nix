{ inputs, lib, pkgs, config, ... }:
with lib;
let
  cfg = config.services.myoffice;
in {
  options.services.myoffice = {
    enable = mkEnableOption "standard 'office' software";
  };
  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      libreoffice
      pandoc
      tectonic texlivePackages.dvisvgm ghostscript
      zotero_7
      inputs.nixpkgs-unstable.legacyPackages.x86_64-linux.typst
    ];
  };
}
