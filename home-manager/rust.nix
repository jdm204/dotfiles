{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.services.myrust;
in {
  options.services.myrust = {
    enable = mkEnableOption "my rust configuration";
  };
  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      rustup
    ];
  };
}
