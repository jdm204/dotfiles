{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.services.myDAW;
in {
  options.services.myDAW = {
    enable = mkEnableOption "Bitwig audio production";
  };
  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      bitwig-studio4 yabridge yabridgectl vital
    ];
  };
}
