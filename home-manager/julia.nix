{ inputs, lib, pkgs, config, ... }:
with lib;
let
  cfg = config.services.myjulia;
in {
  options.services.myjulia = {
    enable = mkEnableOption "my julia configuration";
  };
  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      inputs.nixpkgs-unstable.legacyPackages.x86_64-linux.julia_111-bin
    ];
    home.file.".julia/config/startup.jl".source = ../shell/startup.jl;
  };
}
