{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.services.myshell;
  nixenvdirenv = pkgs.writeShellScriptBin "nixenvdirenv" ''
    echo "use nix" > .envrc
    direnv allow
  '';
  nixenvR = pkgs.writeShellScriptBin "nixenvR" ''
    cp $HOME/dotfiles/shell/R-shell.nix shell.nix
    nixenvdirenv
  '';
  nixenvscanpy = pkgs.writeShellScriptBin "nixenvscanpy" ''
    cp $HOME/dotfiles/shell/scanpy-shell.nix shell.nix
    nixenvdirenv
  '';
in {
  options.services.myshell = {
    enable = mkEnableOption "my command-line shell config";
  };
  config = mkIf cfg.enable {
    
    programs.bash = {
      enable = true;
      shellAliases = {
        ".."="cd ..";
        ll="eza -la --git --group-directories-first --color-scale --icons --sort=ext";
        lll="eza -UHhlagim --git --group-directories-first --color-scale --icons";
        lt="eza -T --icons --group-directories-first --sort=ext";
        r="rip";
        rmb="rm *~ \#*";
        aptu="sudo apt update && sudo apt upgrade";
        apti="sudo apt install";
        apts="apt search";
        apto="apt show";
        l="l";
      };
      initExtra = builtins.readFile ../shell/bashrc.sh;
    };

    programs.zoxide.enable = true;
    
    programs.starship = {
      enable = true;
      settings = {
        scan_timeout = 10;
        character = {
          success_symbol = "➜";
          error_symbol = "➜";
        };
        conda = {
          disabled = false;
          ignore_base = false;
        };
        memory_usage = {
          disabled = false;
          threshold = 50;
          symbol = "mem ";
        };
        cmd_duration = {
          min_time = 500;
          format = "took [$duration](bold)";
        };
      };
    };
    
    home.packages = with pkgs; [
      ripgrep just fd sd du-dust eza xsv bat duf bottom hyperfine gcc miniserve
      gh glab
      unzip wget
      tldr
      magic-wormhole age cosign
      # shell scripts
      nixenvdirenv nixenvR nixenvscanpy
    ];
  };
}
