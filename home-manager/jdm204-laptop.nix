{
  inputs,
  outputs,
  lib,
  config,
  pkgs,
  ...
}: {
  imports = [
    ./emacs.nix
    ./DAW.nix
    ./R.nix
    ./firefox.nix
    ./fonts.nix
    ./git.nix
    ./julia.nix
    ./rust.nix
    ./imageediting.nix
    ./office.nix
    ./shell.nix
    ./TOTP.nix
  ];

  nixpkgs = {
    overlays = [
      inputs.emacs-overlay.overlay
    ];
    config = {
      allowUnfree = true;
    };
  };

  services = {
    myshell.enable = true;
    myemacs.enable = true;
    myjulia.enable = true;
    myrust.enable = false; # prefer dev shells
    myR.enable = false;
    mygit.enable = true;
    myoffice.enable = true;
    myDAW.enable = true;
    myfonts.enable = true;
    myimageediting.enable = true;
    myTOTP.enable = true;
  };

  programs.direnv = {
      enable = true;
      enableBashIntegration = true;
      nix-direnv.enable = true;
  };

  programs.jujutsu = {
    enable = true;
    settings = {
      user = {
        name = "jdm204";
        email = "jdm204@mail.com";
      };
      ui.default_command = "log";
    };
  };

  programs.ssh = {
    enable = true;
    controlMaster = "auto";
    forwardAgent = true;
    controlPersist = "480m";
  };

  home = {
    username = "jdm204";
    homeDirectory = "/home/jdm204";
    file."proj/.keep".text = "";

    packages = with pkgs; [
      nextcloud-client bitwarden
      vlc spotify
      shotwell evince eog
      monolith
      transmission_4-gtk
      davinci-resolve ffmpeg
      igv
      obs-studio
      inputs.nixpkgs-unstable.legacyPackages.x86_64-linux.deno
      (pkgs.buildFHSUserEnv {
        name = "pixi";
        runScript = "pixi";
        targetPkgs = pkgs: with pkgs; [ pixi ];
      })
    ];
  };

  home.sessionVariables = {
    NIXPKGS_ALLOW_UNFREE = 1;
  };

  programs.home-manager.enable = true;

  systemd.user.startServices = "sd-switch";

  # https://nixos.wiki/wiki/FAQ/When_do_I_update_stateVersion
  home.stateVersion = "23.05";
}
