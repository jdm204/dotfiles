{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.services.mygit;
in {
  options.services.mygit = {
    enable = mkEnableOption "git version control";
  };
  config = mkIf cfg.enable {
    programs.git = {
      userName = "jdm204";
      userEmail = "jdm204@mail.com";
      enable = true;
      lfs.enable=true;
      extraConfig = {
        core = { editor = "emacs"; };
        init = { defaultBranch = "main"; };
        pull = { rebase = true; };
        push = {
          autoSetupRemote = true;
          followTags = true;
        };
        rebase = {
          autoSquash = true;
          autoStash = true;
          updateRefs = true;
        };
        fetch = {
          prune = true;
          pruneTags = true;
          all = true;
        };
        merge = { conflictStyle = "zdiff3"; };
        gitlab = { user = "jdm204"; };
        github = { user = "jdm204"; };
        credential."https://gitlab.com" = {
          username = "jdm204";
          helper = "!glab auth git-credential";
        };
        credential."https://github.com" = {
          username = "jdm204";
          helper = "!gh auth git-credential";
        };
        column = { ui = "auto"; };
        branch = { sort = "-committerdate"; };
        tag = { sort = "version:refname"; };
        diff = {
          algorithm = "histogram";
          colorMoved = "plain";
          renames = true;
          mnemonicPrefix = true;
        };
      };
      ignores = [
        "*~"
        "\#*"
        "*\#"
        "target/"
        ".\#*"
        ".DS_Store"
        "texput.*"
        ".aux"
        ".fdb_latexmk"
        "ltximg/"
      ];
    };
    home.packages = with pkgs; [
      delta
    ];
  };
}
