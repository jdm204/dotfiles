{ pkgs, lib, config, ... }:
with pkgs; with lib;
let
  CytoExploreRData = pkgs.rPackages.buildRPackage {
    name = "CytoExploreRData";
    src = pkgs.fetchFromGitHub {
      owner = "DillonHammill";
      repo = "CytoExploreRData";
      rev = "488edf083092247ad547172906efe6f8c2aa8700";
      sha256 = "444DwjSmKEOmGNulDjjgLfHuDnBBnSt/+fqeshlq41Y=";
    };
  };
  CytoExploreR = pkgs.rPackages.buildRPackage {
    name = "CytoExploreR";
    src = pkgs.fetchFromGitHub {
      owner = "DillonHammill";
      repo = "CytoExploreR";
      rev = "0efb1cc19fc701ae03905cf1b8484c1dfeb387df";
      sha256 = "e+BbbD9MqIUMPoPSHusH19QvAdLW4FZlPiSW4HVT7fg=";
    };
    propagatedBuildInputs =  with rPackages; [ flowCore flowWorkspace openCyto BiocGenerics bslib data_table dplyr EmbedSOM flowAI gtools MASS magrittr purrr robustbase rhandsontable Rtsne rsvd shiny superheat tibble tidyr umap visNetwork ];
    #nativeBuildInputs = [ rlang knitr ];
  };
  infercnv = pkgs.rPackages.buildRPackage {
    name = "infercnv";
    src = pkgs.fetchFromGitHub {
      owner = "broadinstitute";
      repo = "infercnv";
      rev = "124eec089e5d9ab5a2a2352461d03db6cdcf0ea0";
      sha256 = "Gpmzl6srWzWKsSLslo2m3OZ7P5/n0dttDuhBUFOl8NE=";
    };
    propagatedBuildInputs =  with rPackages; [ python3 RColorBrewer gplots futile_logger ape phyclust Matrix fastcluster parallelDist dplyr HiddenMarkov ggplot2 edgeR coin caTools digest RANN igraph reshape2 rjags fitdistrplus future foreach doParallel Seurat BiocGenerics SummarizedExperiment SingleCellExperiment tidyr coda gridExtra argparse ];
    #nativeBuildInputs = [ rlang knitr ];
  };
  copykat = pkgs.rPackages.buildRPackage {
    name = "copykat";
    src = pkgs.fetchFromGitHub {
      owner = "navinlabcode";
      repo = "copykat";
      rev = "d7d6569ae9e30bf774908301af312f626de4cbd5";
      sha256 = "y+ZjXwKgfP2ovonAnsUEgNkrUW6MlHu2j1T75bKv9PM=";
    };
    propagatedBuildInputs =  with rPackages; [ parallelDist dlm gplots RColorBrewer mixtools cluster MCMCpack ];
    #nativeBuildInputs = [ rlang knitr ];
  };
  yaGST = pkgs.rPackages.buildRPackage {
    name = "yaGST";
    src = pkgs.fetchFromGitHub {
      owner = "miccec";
      repo = "yaGST";
      rev = "56227df3ae183070c9d156af11c306ee799435e6";
      sha256 = "7WFcz88dsbz/MHKnK4Bc/gp1T1zgwMAfuOvPks0W/JU=";
    };
    propagatedBuildInputs =  with rPackages; [ doParallel ggplot2 ];
    #nativeBuildInputs = [ rlang knitr ];
  };
  SCEVAN = pkgs.rPackages.buildRPackage {
    name = "SCEVAN";
    src = pkgs.fetchFromGitHub {
      owner = "AntonioDeFalco";
      repo = "SCEVAN";
      rev = "92761f664dde885b04b4257d6b5250ffe0a8247b";
      sha256 = "1wovfVZHjw1p5tFlsIlGChxRciLTJekAh1g7YyzIVuY=";
    };
    propagatedBuildInputs =  with rPackages; [ yaGST parallelDist pheatmap forcats dplyr fgsea cluster Rtsne scran ape ggtree tidytree ggrepel ];
  };
  presto = pkgs.rPackages.buildRPackage {
    name = "presto";
    src = pkgs.fetchFromGitHub {
      owner = "immunogenomics";
      repo = "presto";
      rev = "7636b3d0465c468c35853f82f1717d3a64b3c8f6";
      sha256 = "Sfjx5e0drUrRA9f0gxmeN8Z3cdz6Q3LBVWM3tV6k8R0=";
    };
    propagatedBuildInputs =  with rPackages; [ Rcpp data_table dplyr tidyr purrr tibble Matrix rlang RcppArmadillo ];
  };
  SeuratDisk = pkgs.rPackages.buildRPackage {
    name = "SeuratDisk";
    src = pkgs.fetchFromGitHub {
      owner = "mojaveazure";
      repo = "seurat-disk";
      rev = "877d4e18ab38c686f5db54f8cd290274ccdbe295";
      sha256 = "tQXes2KRHFpH8mSY4DCdqBHzcMx0okt1SbN6XdLESVU=";
    };
    propagatedBuildInputs =  with rPackages; [ cli crayon hdf5r Matrix R6 rlang Seurat SeuratObject stringi withr ];
  };
  R-with-my-packages = rWrapper.override { packages = with rPackages; [
    tidyverse readxl ggrepel  patchwork ggpubr
    BiocManager devtools hdf5r DESeq2
    Seurat SeuratDisk presto glmGamPoi enrichR
    ape GSVA tximeta EnsDb_Hsapiens_v86 ensembldb
    GEOquery survminer MASS
    # cytoexplorer
    remotes latticeExtra XML BiocManager scattermore flowAI
    cytolib flowCore flowWorkspace flowCore flowClust openCyto CytoML fftw
    CytoExploreRData CytoExploreR
    INSPECTumours markdown
    AER rstatix
    infercnv copykat yaGST SCEVAN
    drc
    irr
    brms
  ]; };
  cfg = config.services.myR;
in
{
  options.services.myR = {
    enable = mkEnableOption "my R configuration";
  };
  config = mkIf cfg.enable {
    home.packages = [
      R-with-my-packages
      gnumake
    ];
    home.sessionVariables = {
      EXTRA_CCFLAGS = "-I/usr/include";
      LD_LIBRARY_PATH="${pkgs.R}/lib/R/lib:$LD_LIBRARY_PATH";
      R_HOME="${pkgs.R}/lib/R/";
      LIBRARIES="$(Rscript -e 'paste(.libPaths(), collapse=\":\")')";
      R_LIBS_SITE="$(echo $LIBRARIES | cut -c6- | rev | cut -c2- | rev)";
    };
  };
}
