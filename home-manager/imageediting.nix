{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.services.myimageediting;
in {
  options.services.myimageediting = {
    enable = mkEnableOption "programs for working with digital images";
  };
  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      inkscape krita imagemagick rnote
    ];
  };
}
