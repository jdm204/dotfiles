{ lib, pkgs, config, ... }:
with lib;
let
  cfg = config.services.myTOTP;
  gettotp = pkgs.writeShellScriptBin "gettotp.sh" ''
      gpg -d ~/.gnupg/totp.secret | cotp --password-stdin
    '';
  showtotp = pkgs.writeShellScriptBin "showtotp.sh" ''
      cosmic-term -- gettotp.sh
    '';
in {
  options.services.myTOTP = {
    enable = mkEnableOption "TOTPs for authentication";
  };
  config = mkIf cfg.enable {
    home.packages = with pkgs; [
      cotp gettotp showtotp
    ];
  };
}
