_default:
    @just --list -u

home:
    home-manager switch --flake .#jdm204@laptop
nixos:
    sudo nixos-rebuild switch --flake .#prec3581
update:
    nix flake update
gc:
    nix-collect-garbage

upgrade: update nixos home
