#import "@preview/polylux:0.3.1": *
#import "./metropolis-theme.typ": *
#show: metropolis-theme
#set text(font: "IBM Plex Sans", weight: "light", size: 20pt)
#set strong(delta: 100)
#set par(justify: true)

#title-slide(
  author: [Dr Jamie D Matthews],
  title: "Slides Main Title",
  subtitle: "Slides Subtitle: A presentation",
    date: datetime.today().display("[month repr:long] [day], [year]"),
  extra: "Turner Lab Meeting"
)

#slide(title: "Table of contents")[
  #metropolis-outline
]

#slide(title: "Slide title")[
    A slide with some maths:

    #side-by-side(gutter: 1mm, colums: (1fr, 4fr))[  $ x_(n+1) = (x_n + a/x_n) / 2 $ ][
        - we can do this
        - if we believe
        - well, that's my whole thing
    ]
]

#new-section-slide("First section")

#slide[
  A slide without a title but with #alert[important] infos
]

#new-section-slide([Second section])

#focus-slide[
  Wake up!
]
