function signaljulia {
    kill -SIGUSR1 $(pidof julia)
}

export RUSTFLAGS="-Ctarget-cpu=native"

GL=https://gitlab.com/jdm204

[ -n "$EAT_SHELL_INTEGRATION_DIR" ] && \
    source "$EAT_SHELL_INTEGRATION_DIR/bash"

function msg {
    echo $(tput smso)$(tput setaf 163)"$*"$(tput sgr0)
}

function functions {
    if [[ $# -eq 0 ]]; then
        msg aliases
        rg "^alias" $HOME/.bashrc | cut -d ' ' -f 2 | cut -d '=' -f 1 | tr "\n" " " && echo

        msg functions
        rg "^function" $HOME/.bashrc | cut -d ' ' -f 2 | tr "\n" " " && echo
    else
        type $1
    fi
}

PATH=~/.local/bin:$PATH

function isrepoclean {
    statusResult=$(git status -u --porcelain)
    if [ -z statusResult ]
    then
        echo 'no changes found'
    else
        echo 'The workspace is modified:'
        echo "$statusResult"
    fi
}

# displaying TOTPs
function totp {
    gpg -d ~/.gnupg/totp.secret | cotp --password-stdin
}

function l {
  if [[ $# -eq 0 ]]; then
      eza -F --icons --group-directories-first --sort=ext
  else
      case $1 in
          *.csv)
              xsv table $* | less -S
              ;;
          *.tsv)
              xsv table -d '\t' $* | less -S
              ;;
          --tsv|-t)
              shift; xsv table -d '\t' $* | less -S
              ;;
          --csv|-c)
              shift; xsv table $* | less -S
              ;;
          *)
              if [[ -d $1 ]]; then
                  eza -F --icons --group-directories-first --sort=ext "$@"
              else
                  bat --theme=OneHalfLight --wrap=never --style=header "$@"
              fi
              ;;
      esac
  fi
}

function open {
    for arg in "$@"
    do
        xdg-open "$arg"
    done
}

function jpc {
    ssh -Y $* jamie@172.27.47.59
}

function cloudpc {
    ssh zesgmpkq@46.165.196.66 -p 60504 -i ~/.ssh/cloudamokey
}

function jhpc {
    ssh -Y $* jdm204@login-cpu.hpc.cam.ac.uk
}

function jhpcgpu {
    ssh -Y $* jdm204@login-gpu.hpc.cam.ac.uk
}

function jscp {
    case $1 in
        pull)
            scp jamie@172.27.47.59:$2 $3 ;;
        push)
            scp $2 jamie@172.27.47.59:$3 ;;
        dpull)
            scp -r jamie@172.27.47.59:$2 $3 ;;
        dpush)
            scp -r $2 jamie@172.27.47.59:$3 ;;
        *)
            msg "pull or push! equally, dpull or dpush for recursive copying!" ;;
    esac
}

function jhpcscp {
    case $1 in
        pull)
            scp jdm204@login-cpu.hpc.cam.ac.uk:$2 $3 ;;
        push)
            scp $2 jdm204@login-cpu.hpc.cam.ac.uk:$3 ;;
        dpull)
            scp -r jdm204@login-cpu.hpc.cam.ac.uk:$2 $3 ;;
        dpush)
            scp -r $2 jdm204@login-cpu.hpc.cam.ac.uk:$3 ;;
        *)
            msg "Usage: jhpcscp {pull,push,dpull,dpush} PATH1 PATH2" ;;
    esac
}

function helix {
    chimerax --cmd "set bgColor white; graphics silhouettes true; build start nucleic 'DNA' ${1} type dna form B; hbonds reveal true"
}

function peptide {
    phipsi=$(printf "%${#1}s" | sed 's/ /-57,-47 /g')
    chimerax --cmd "set bgColor white; graphics silhouettes true; build start peptide Peptide ${1} ${phipsi}; hbonds reveal true"
}

function pluto {
     julia -e "import Pluto; Pluto.run()" &
}

function bwp {
    if [[ -z $BW_SESSION ]]
    then
        export BW_SESSION="$(bw unlock --raw)"
    fi
    bw get password $1 | xclip -selection c
}

function newjuliapkg {
    line="newpkg!(\"${1}\")"
    julia -e $line
}

function msg {
      echo $(tput smso)$(tput setaf 163)"$*"$(tput sgr0)
}

function warn {
      echo $(tput smso)$(tput setaf 196)"$*"$(tput sgr0)
}

function ensure_link {
    if [[ -L $2 ]]
    then
        warn $2 is already a link, quitting
        return 1
    elif [[ -e $2 ]]
    then
         warn $2 exists and is a file, quitting
         return 1
    else
        ln -s $1 $2 && msg linked $1 to $2
    fi
}

function missing_command {
    if command -v $1 &> /dev/null
    then
        return 1
    else
        return 0
    fi
}

function ensure_dirs {
    mkdir -p $*
}
