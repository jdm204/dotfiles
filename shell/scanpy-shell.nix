let
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/0b73e36b1962620a8ac551a37229dd8662dac5c8.tar.gz") {};

  anndata = (pkgs.python3Packages.buildPythonPackage rec {
    pname = "anndata";
    version = "0.11.3";
    pyproject = true;
    src = pkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-mG5ia2IQjo23A02y85H9FgwR1LTjr5wxGDyChiKX9/Y=";
    };
    propagatedBuildInputs = with pkgs.python3Packages; [
      hatchling hatch-vcs natsort h5py array-api-compat pandas numpy scipy
    ];
    doCheck = false;
    pythonImportsCheck = [ "anndata" ];
  });
  hatch-docstring-description = (pkgs.python3Packages.buildPythonPackage rec {
    pname = "hatch-docstring-description";
    version = "1.0.3";
    pyproject = true;
    src = pkgs.fetchPypi {
      pname = "hatch_docstring_description";
      inherit version;
      hash = "sha256-4EeZf67ZyGcl7IjXV07iCNkJmOjTIvuWxIXD1QKyYpE=";
    };
    propagatedBuildInputs = with pkgs.python3Packages; [ hatchling hatch-vcs ];
    doCheck = false;
  });
  session-info2 = (pkgs.python3Packages.buildPythonPackage rec {
    pname = "session-info2";
    version = "0.1.2";
    pyproject = true;
    src = pkgs.fetchPypi {
      pname = "session_info2";
      inherit version;
      hash = "sha256-vbdYhRKDM2EeDPQ1fzGgtYaby3iJuTJR7YVxqk5K0LY=";
    };
    propagatedBuildInputs = with pkgs.python3Packages; [ hatchling hatch-vcs hatch-docstring-description ];
    doCheck = false;
  });
  legacy-api-wrap = (pkgs.python3Packages.buildPythonPackage rec {
    pname = "legacy-api-wrap";
    version = "1.4.1";
    pyproject = true;
    src = pkgs.fetchPypi {
      pname = "legacy_api_wrap";
      inherit version;
      hash = "sha256-nEDWeqgxL+yHY+h8vyj+pLZ3EMecp6GBN7Vz0VDzsrQ=";
    };
    propagatedBuildInputs = with pkgs.python3Packages; [ hatchling hatch-vcs hatch-docstring-description ];
    doCheck = false;
  });
  scanpy = (pkgs.python3Packages.buildPythonPackage rec {
    pname = "scanpy";
    version = "1.11.0";
    pyproject = true;

    src = pkgs.fetchPypi {
      inherit pname version;
      hash = "sha256-9Bvb1u44Nt6KTrXAgo29yw+wFJMOrLsASbN7y4SnTVQ=";
    };

    propagatedBuildInputs = with pkgs.python3Packages; [
      anndata hatchling hatch-vcs natsort h5py array-api-compat pandas numpy scipy
      joblib matplotlib networkx numba patsy pynndescent scikit-learn seaborn
      statsmodels tqdm umap-learn session-info2 legacy-api-wrap
    ];

    doCheck = false;
    pythonImportsCheck = [ "scanpy" ];
  });

in pkgs.mkShell {
  packages = [
    (pkgs.python3.withPackages (python-pkgs: [
      # select Python packages here
      python-pkgs.pandas
      python-pkgs.requests
      scanpy
    ]))
  ];
}
