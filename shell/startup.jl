using Revise
import JuliaFormatter, PkgTemplates, Pkg
import Coverage
import Infiltrator: @infiltrate
import Chain: @chain as @|
newpkg! = PkgTemplates.Template(;
                                dir="~/proj",
                                user="jdm204",
                                host="gitlab.com",
                                plugins=[
                                    !PkgTemplates.GitHubActions,
                                    !PkgTemplates.TagBot,
                                    !PkgTemplates.CompatHelper,
                                    PkgTemplates.GitLabCI(),
                                    PkgTemplates.Documenter{PkgTemplates.GitLabCI}(),
                                ])
function coverage!()
    Coverage.clean_folder("src")
    Pkg.test(coverage=true)
    covered, total = Coverage.get_summary(Coverage.process_folder())
    pc = covered / total * 100
    println("Coverage: $(pc)%")
end
projfmt!(root=Pkg.project().path |> dirname) = JuliaFormatter.format(
    root, JuliaFormatter.SciMLStyle())
