let
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/0b73e36b1962620a8ac551a37229dd8662dac5c8.tar.gz") {};
in
pkgs.mkShell {
  nativeBuildInputs = with pkgs.buildPackages; with rPackages; [
    R tidyverse ggrepel patchwork
  ];
}
