let
  pkgs = import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/0b73e36b1962620a8ac551a37229dd8662dac5c8.tar.gz") {};
  CytoExploreRData = pkgs.rPackages.buildRPackage {
    name = "CytoExploreRData";
    src = pkgs.fetchFromGitHub {
      owner = "DillonHammill";
      repo = "CytoExploreRData";
      rev = "488edf083092247ad547172906efe6f8c2aa8700";
      sha256 = "444DwjSmKEOmGNulDjjgLfHuDnBBnSt/+fqeshlq41Y=";
    };
  };
  CytoExploreR = pkgs.rPackages.buildRPackage {
    name = "CytoExploreR";
    src = pkgs.fetchFromGitHub {
      owner = "DillonHammill";
      repo = "CytoExploreR";
      rev = "0efb1cc19fc701ae03905cf1b8484c1dfeb387df";
      sha256 = "e+BbbD9MqIUMPoPSHusH19QvAdLW4FZlPiSW4HVT7fg=";
    };
    propagatedBuildInputs =  with pkgs.rPackages; [ flowCore flowWorkspace openCyto BiocGenerics bslib data_table dplyr EmbedSOM flowAI gtools MASS magrittr purrr robustbase rhandsontable Rtsne rsvd shiny superheat tibble tidyr umap visNetwork ];
  };
in
pkgs.mkShell {
  packages = with pkgs; [
    R
    rPackages.tidyverse
    rPackages.ggrepel
    rPackages.patchwork
    CytoExploreR CytoExploreRData
  ];
}
